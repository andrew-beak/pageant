# Project notes

# Backend design pattern

I've implemented the basis of the event sourcing pattern.  I chose this because it provides a natural audit of the changes, and also allows for more flexible reporting in the future.  For example, we may want to identify organisations with high staff turnover and this becomes possible when using this pattern.

I implemented it in Laravel because time was a factor.  I was entirely unfamiliar with the event sourcing pattern and this is the first time that I've used it.  I wanted to use a familiar framework to minimise the risks involved in implementing a new pattern.

I've used a SOLID approach to the domain logic and encapsulated it into a "Domain" namespace.

# REST

Purists argue that an API is not properly RESTful if it does not use HATEOAS.  The idea being that if state is persisted in the client then you're not adhering to REST.  The problem is that HATEOAS is a PITA and in my experience it is ignored by client developers.

I've used a resourceful controller, which is a Laravel way of mapping HTTP actions to the appropriate REST verbs.  This also encourages a semantic URL format.

Therefore, this is not a RESTful API, but it does use url paths and HTTP verbs in a way that most people will say is "close enough to RESTful"

The approach to URL naming that I really like is the one described at https://jsonapi.org/recommendations/#urls-relationships

# Running the app

The API can be run using the supplied docker-compose file in `api/docker/docker-compose.yml`.

The frontend can be run manually by running `npm start` in `frontend`

# Security

The frontend could be secured with OAuth2 provided by a provider like Cognito, Azure, Auth0, or Okta.

The backend would be protected by two layers:

1) A gateway service like Kong enterprise or AWS API Gateway would allow developer signup and manage authorisation on the edge
2) The API itself will implement middleware that performs authorisation on the request itself

The gateway answers the question "Have you paid for this service?"

The application answers the question "Are you allowed to perform this action or access that data?"

Implementing authorisation in the application would be done in the middleware, most probably with Passport (on Laravel at least) which wraps the League of Extraordinary packages OAuth2 library.

This application is currently vulnerable to stored XSS attacks as there is no encoding performed on the backend.  I'm expecting that it is also susceptible to DOM XSS attacks.

Currently the frontend and backend are hosted on the same IP address so CORS is not needed.  If they were not then we would need to configure nginx to loosen the same domain protection for the ip address that the frontend is on.  Using a wildcard is a common, but sloppy and insecure, approach to this.

# New database model

Please see `new-model.png`

The organisation had a `country_id` and so I have created a lookup table to contain country information.  Organisations can exist in many different countries and so there is a many-to-many relationship between countries and organisations.  Likewise, it seems reasonable that we would want to track the contact details for a company in each of the countries in which they operate, so organisation contact details are promoted to become an entity that is modeled in its own table.

People's jobs are modeled as "roles" which links people to organisations.  The "title" field has been removed from the `person` entity and placed into the role.  This reflects that people may have different job titles at each of the organisations they work for.

People are not split amongst multiple countries and their phone number should include a dialling prefix.  A comment is placed on the database column to document this expectation.

I have duplicated the "phone" field in the role and in the person.  The expectation is that the person may have their own mobile phone, and also have a work number associated with a role.  It was highlighted that it is important to contact people so having the ability to store additional contact information seems important.
Looking at this now I think I should also do this with "email" and will refactor the code if I get the time.

## UUID

I have not applied UUID fields consistently in the project.  This is because of the limited time.  Having a UUID field in the entity is advantageous when creating the entity, but it does feel hacky and possibly even a violation of database design principals.

It's advantageous because the patterns aims for eventual consistency and when you call the service to create an entity you're not able to get the PK of the row involved.  This makes it more difficult to do things like linking related records.

Having the code return a database auto-incremented PK seems risky at first glance because race conditions could lead to problems in high concurrency.

## Migrating the database

I have included a seed script to create the legacy database, obviously in production this won't be needed and a production environment config won't include a connection to the legacy database (and especially not one using a root account - time limits me from scripting a less privileged user).

Docker will run the seed script when you bring up the mysql container for the first time.

I have created a console command "pageant:migrate" that reads the legacy database and prepares events.  We can then play these events using the event sourcing
code to create the database.  Obviously, we do not know exactly what was changed at the time of the "modified on" timestamp database field, but we are capturing that there was a change made at that time by the user involved.

The `migrate.sh` script will run the migration for you.