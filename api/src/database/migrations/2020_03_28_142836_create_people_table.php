<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');            
            $table->string('name', 100);            
            $table->string('phone', 45)->nullable()->comment('Should include international dialling prefix');
            $table->string('email', 100)->nullable();
            $table->boolean('active');            
            $table->softDeletes('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('people');
        Schema::enableForeignKeyConstraints();
    }
}
