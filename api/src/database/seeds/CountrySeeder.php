<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                'id' => 127,
                'name' => 'Canada',
                'iso2' => 'CA',
                'dialling_prefix' => 1
            ],
            [
                'id' => 173,
                'name' => 'Australia',
                'iso2' => 'AU',
                'dialling_prefix' => 61
            ],
            [
                'id' => 61,
                'name' => 'United States',
                'iso2' => 'CA',
                'dialling_prefix' => 1
            ],
            [
                'id' => 158,
                'name' => 'Austria',
                'iso2' => 'AT',
                'dialling_prefix' => 43
            ]
        ];

        foreach ($countries as $country) {
            DB::table('countries')->insert($country);
        }
        
    }
}
