<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DomainConcretionProvider extends ServiceProvider
{
    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            // abstract
            \App\Domain\Interfaces\OrganisationRepositoryInterface::class,
            // concrete
            \App\Domain\Repositories\OrganisationRepository::class
        );

        $this->app->bind(
            \App\Domain\Interfaces\PersonRepositoryInterface::class,
            \App\Domain\Repositories\PersonRepository::class
        );

        $this->app->bind(
            \App\Domain\Interfaces\OrganisationServiceInterface::class,
            \App\Domain\Services\OrganisationService::class
        );

        $this->app->bind(
            \App\Domain\Interfaces\CountryRepositoryInterface::class,
            \App\Domain\Repositories\CountryRepository::class
        );

        $this->app->bind(
            \App\Domain\Interfaces\PersonServiceInterface::class,
            \App\Domain\Services\PersonService::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
