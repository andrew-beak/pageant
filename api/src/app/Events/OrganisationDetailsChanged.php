<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class OrganisationDetailsChanged implements ShouldBeStored
{
    /**
     * @var int
     */
    public int $organisationId;

    /**
     * @var array
     */
    public array $newDetails;

    /**
     * OrganisationDetailsChanged constructor.
     * @param int $organisationId
     * @param array $newDetails
     */
    public function __construct(int $organisationId, array $newDetails)
    {
        $this->organisationId = $organisationId;

        $this->newDetails = $newDetails;
    }
}
