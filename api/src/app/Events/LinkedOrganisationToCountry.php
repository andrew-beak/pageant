<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class LinkedOrganisationToCountry implements ShouldBeStored
{
    /**
     * @var int
     */
    public int $organisationId;

    /**
     * @var int
     */
    public int $countryId;

    /**
     * LinkedOrganisationToCountry constructor.
     * @param int $organisationId
     * @param int $countryId
     */
    public function __construct(int $organisationId, int $countryId)
    {
        $this->organisationId = $organisationId;

        $this->countryId = $countryId;
    }
}
