<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class PersonCreated implements ShouldBeStored
{
    /**
     * @var array
     */
    public array $attributes;

    /**
     * PersonCreated constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }
}
