<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class OrganisationDetailsAdded implements ShouldBeStored
{
    /**
     * @var array
     */
    public array $newDetails;

    /**
     * @var int
     */
    public int $organisationId;

    /**
     * @var int
     */
    public int $countryId;

    /**
     * OrganisationDetailsAdded constructor.
     * @param array $newDetails
     * @param int $organisationId
     * @param int $countryId
     */
    public function __construct(array $newDetails, int $organisationId, int $countryId)
    {
        $this->newDetails = $newDetails;

        $this->organisationId = $organisationId;

        $this->countryId = $countryId;
    }
}
