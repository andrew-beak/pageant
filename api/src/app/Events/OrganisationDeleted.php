<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class OrganisationDeleted implements ShouldBeStored
{
    /**
     * @var int
     */
    public int $organisationId;

    /**
     * OrganisationDeleted constructor.
     * @param int $organisationId
     */
    public function __construct(int $organisationId)
    {
        $this->organisationId = $organisationId;
    }
}
