<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class OrganisationCreated implements ShouldBeStored
{
    /**
     * @var array
     */
    public array $attributes;

    /**
     * OrganisationCreated constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
    }
}
