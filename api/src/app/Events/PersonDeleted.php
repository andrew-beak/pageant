<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class PersonDeleted implements ShouldBeStored
{
    /**
     * @var int|string
     */
    public string $id;

    /**
     * PersonDeleted constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
