<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class PersonDetailsChanged implements ShouldBeStored
{
    /**
     * @var int|string
     */
    public string $id;

    /**
     * @var array
     */
    public array $newDetails;

    /**
     * PersonDetailsChanged constructor.
     * @param int $id
     * @param array $newDetails
     */
    public function __construct(int $id, array $newDetails)
    {
        $this->id = $id;

        $this->newDetails = $newDetails;
    }
}
