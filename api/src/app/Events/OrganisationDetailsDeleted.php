<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class OrganisationDetailsDeleted implements ShouldBeStored
{
    /**
     * @var string
     */
    public string $organisationId;

    /**
     * OrganisationDetailsDeleted constructor.
     * @param string $organisationId
     */
    public function __construct(string $organisationId)
    {
        $this->organisationId = $organisationId;
    }
}
