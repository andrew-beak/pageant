<?php

namespace App\Events;

use Spatie\EventSourcing\ShouldBeStored;

class LinkedPersonToOrganisation implements ShouldBeStored
{
    /**
     * @var int
     */
    public int $personId;

    /**
     * @var int
     */
    public int $organisationId;

    /**
     * @var string
     */
    public ?string $jobTitle;

    /**
     * @var string
     */
    public ?string $phone;

    /**
     * LinkedPersonToOrganisation constructor.
     * @param int $personId
     * @param int $organisationId
     */
    public function __construct(int $personId, int $organisationId, ?string $jobTitle, ?string $phone)
    {
        $this->personId = $personId;

        $this->organisationId = $organisationId;

        $this->jobTitle = $jobTitle;

        $this->phone = $phone;
    }
}
