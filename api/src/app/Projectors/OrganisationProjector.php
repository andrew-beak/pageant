<?php

namespace App\Projectors;

use App\Events\LinkedOrganisationToCountry;
use App\Events\OrganisationCreated;
use App\Events\OrganisationDeleted;
use App\Events\OrganisationDetailsAdded;
use App\Events\OrganisationRemovedFromCountry;
use App\Domain\Persistence\Organisation;
use App\Domain\Persistence\OrganisationContactDetail;
use Spatie\EventSourcing\Models\StoredEvent;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;
use Spatie\EventSourcing\StoredEventRepository;

class OrganisationProjector implements Projector
{
    use ProjectsEvents;

    /**
     * @param OrganisationCreated $event
     */
    public function onOrganisationCreated(OrganisationCreated $event): void
    {
        Organisation::create($event->attributes)->save();
    }

    /**
     * @param LinkedOrganisationToCountry $event
     */
    public function onLinkedOrganisationToCountry(LinkedOrganisationToCountry $event): void
    {
        $organisation = Organisation::find($event->organisationId);
        $organisation->countries()->attach($event->countryId);
    }

    /**
     * @param OrganisationRemovedFromCountry $event
     */
    public function onOrganisationRemovedFromCountry(OrganisationRemovedFromCountry $event): void
    {
        $organisation = Organisation::find($event->organisationId);
        $organisation->countries()->detach($event->countryId);
    }

    /**
     * @param OrganisationDetailsAdded $event
     */
    public function onOrganisationDetailsAdded(OrganisationDetailsAdded $event): void
    {
        $attributes = $event->newDetails;
        $attributes['country_id'] = $event->countryId;
        $attributes['organisation_id'] = $event->organisationId;
        $organisationContactDetail = OrganisationContactDetail::create($attributes);
    }

    /**
     * @param OrganisationDeleted $event
     */
    public function onOrganisationDeleted(OrganisationDeleted $event): void
    {
        Organisation::find('id', $event->organisationId)->delete();
    }
}
