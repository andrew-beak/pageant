<?php

namespace App\Projectors;

use App\Events\PersonCreated;
use App\Events\PersonDeleted;
use App\Events\LinkedPersonToOrganisation;
use App\Domain\Persistence\Person;
use App\Domain\Persistence\Organisation;
use App\Domain\Persistence\Role;
use Spatie\EventSourcing\Models\StoredEvent;
use Spatie\EventSourcing\Projectors\Projector;
use Spatie\EventSourcing\Projectors\ProjectsEvents;
use Spatie\EventSourcing\StoredEventRepository;

class PersonProjector implements Projector
{
    use ProjectsEvents;

    /**
     * @param PersonCreated $event
     */
    public function onPersonCreated(PersonCreated $event): void
    {
        Person::create($event->attributes)->save();
    }

    /**
     * @param StoredEvent $event
     */
    public function onPersonUpdated(StoredEvent $event): void
    {
        $person = Person::findByUuid($event->id);

        $newDetails = $event->newDetails;

        foreach ($newDetails as $newDetail => $newValue) {
            if (isset($person->$newDetail)) {
                $person->$newDetail = $newValue;
            }
        }

        $person->save();
    }

    /**
     * @param PersonDeleted $event
     */
    public function onPersonDeleted(PersonDeleted $event): void
    {
        Person::findOrFail($event->id)->delete();
    }

    /**
     * @param LinkedPersonToOrganisation $event
     */
    public function onLinkedPersonToOrganisation(LinkedPersonToOrganisation $event): void
    {
        Role::create([
            'organisation_id' => $event->organisationId,
            'people_id' => $event->personId,
            'job_title' => $event->jobTitle,
            'phone' => $event->phone
        ])->save();
    }
}
