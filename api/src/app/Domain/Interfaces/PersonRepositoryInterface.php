<?php

namespace App\Domain\Interfaces;
use Illuminate\Database\Eloquent\Collection;

interface PersonRepositoryInterface
{
    /**
     * @param int $id
     * @param array $newAttributes
     */
    public static function changeContactDetails(int $id, array $newAttributes): void;

    public function remove(): void;

    /**
     * @param int $personId
     * @return array
     */
    public function getOrganisationRoleForPerson(int $personId, int $organisationId): array;

    /**
     * @return array
     */
    public function getPeople(): array;
}