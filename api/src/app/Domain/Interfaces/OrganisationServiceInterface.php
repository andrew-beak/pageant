<?php

namespace App\Domain\Interfaces;

use App\Domain\Persistence\Organisation;

interface OrganisationServiceInterface
{

    /**
     * @param int $id
     * @return array
     */
    public function getOrganisationDetails(int $id): array;

    /**
     * @param array $attributes
     * @return Organisation
     */
    public function createWithAttributes(array $attributes): Organisation;

    /**
     * @param int $organisationId
     * @param int $countryId
     */
    public function linkOrganisationToCountry(int $organisationId, int $countryId): void;

    /**
     * @param int $organisationId
     * @param int $countryId
     */
    public function unlinkOrganisationFromCountry(int $organisationId, int $countryId): void;

    /**
     * @param int $organisationId
     * @param array $details
     * @param int $countryId
     */
    public function addContactDetailsForOrganisation(int $organisationId, array $details, int $countryId): void;

    /**
     * @param int $organisationId
     * @param array $details
     */
    public function changeContactDetailsForOrganisation(int $organisationId, array $details): void;

    /**
     * @param int $organisationId
     */
    public function removeContactDetailsForOrganisation(int $organisationId): void;
}