<?php

namespace App\Domain\Interfaces;
use Illuminate\Database\Eloquent\Collection;
use App\Domain\Persistence\Organisation;

interface OrganisationRepositoryInterface
{
    /**
     * @return Illuminate\Database\Eloquent\Collection;
     */
    public function getOrganisations(): array;

    /**
     * @param int $id
     * @return App\Organisation
     */
    public function getOrganisationById(int $id): array;

    /**
     * @param int $id
     * @return array
     */
    public function getCurrentPeople(int $id): array;

    /**
     * @param int $id
     * @return array
     */
    public function getCurrentContactDetails(int $id): array;
}