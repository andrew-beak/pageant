<?php

namespace App\Domain\Interfaces;

interface PersonServiceInterface
{
    /**
     * @param int $id
     * @param array $newAttributes
     */
    public static function changeContactDetails(int $id, array $newAttributes): void;

    public function remove(): void;

}