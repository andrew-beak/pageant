<?php

namespace App\Domain\Interfaces;

interface CountryRepositoryInterface
{
    /**
     * @param int $id
     * @return string
     */
    public function getCountryNameFromId(int $id): string;
}