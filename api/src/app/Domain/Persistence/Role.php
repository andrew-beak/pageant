<?php

namespace App\Domain\Persistence;

use App\Events\PersonCreated;
use App\Events\PersonDeleted;
use App\Events\PersonDetailsChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class Role extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function person()
    {
        return $this->hasOne(Person::class);
    }
}
