<?php

namespace App\Domain\Persistence;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * @return mixed
     */
    public function organisations()
    {
        return $this->belongsToMany(Organisation::class, 'country_organisation', 'country_id', 'organisation_id');
    }
}
