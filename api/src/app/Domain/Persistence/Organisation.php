<?php

namespace App\Domain\Persistence;

use App\Events\LinkedOrganisationToCountry;
use App\Events\OrganisationCreated;
use App\Events\RemovedOrganisationFromCountry;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class Organisation extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $guarded = [];

    /**
     * @param int $organisationId
     * @param int $countryId
     * @return mixed
     */
    public function getOrganisationContactDetailsForCountry(int $organisationId, int $countryId)
    {
        return DB::table('organisation_contact_details')
            ->where('organisation_id', $organisationId)
            ->where('country_id', $countryId)
            ->first();
    }

    /**
     * @param string $uuid
     * @return Organisation
     */
    public function getOrganisationByUuid(string $uuid): Organisation
    {
        return static::where('uuid', $uuid)->first();
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class, 'country_organisation', 'organisation_id', 'country_id');
    }

    public function contactDetails()
    {
        return $this->hasMany(OrganisationContactDetail::class);
    }

    public function people()
    {
        return $this->belongsToMany(Person::class, 'roles', 'organisation_id', 'people_id');
    }
}
