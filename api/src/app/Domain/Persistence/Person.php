<?php

namespace App\Domain\Persistence;

use App\Events\PersonCreated;
use App\Events\PersonDeleted;
use App\Events\PersonDetailsChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid;

class Person extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'people';

    public function roles()
    {
        return $this->hasMany(Role::class);
    }
}
