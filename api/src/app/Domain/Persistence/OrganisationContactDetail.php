<?php

namespace App\Domain\Persistence;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganisationContactDetail extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'organisation_contact_details';

    public function organisation()
    {
        return $this->belongsTo(Organisation::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

}
