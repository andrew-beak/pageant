<?php

namespace App\Domain\Persistence;

use Spatie\EventSourcing\Models\EloquentStoredEvent;

class StoredEvent extends EloquentStoredEvent
{
    public static function boot()
    {
        parent::boot();

        static::creating(
            function (CustomStoredEvent $storedEvent) {
                $userName = auth()->user()->name ?? 'Unknown';
                $storedEvent->meta_data['user_name'] = $userName;
            }
        );
    }
}
