<?php

namespace App\Domain\Services;

use App\Domain\Interfaces\OrganisationServiceInterface;
use App\Domain\Interfaces\OrganisationRepositoryInterface;
use App\Domain\Interfaces\PersonRepositoryInterface;
use App\Domain\Interfaces\CountryRepositoryInterface;
use App\Domain\Persistence\Organisation;

class OrganisationService implements OrganisationServiceInterface
{
    /**
     * @var OrganisationRepositoryInterface
     */
    private OrganisationRepositoryInterface $organisationRepository;

    /**
     * @var CountryRepositoryInterface
     */
    private CountryRepositoryInterface $countryRepository;

    /**
     * @var PersonRepositoryInterface
     */
    private PersonRepositoryInterface $personRepository;

    /**
     * @param OrganisationRepositoryInterface $organisationRepository
     */
    public function __construct(
        OrganisationRepositoryInterface $organisationRepository,
        CountryRepositoryInterface $countryRepository,
        PersonRepositoryInterface $personRepository
    ) {
        $this->organisationRepository = $organisationRepository;

        $this->countryRepository = $countryRepository;

        $this->personRepository = $personRepository;
    }

    /**
     * @inheritDoc
     */
    public function getOrganisationDetails(int $id): array
    {
        $basicInfo = $this->organisationRepository->getOrganisationById($id);
        $currentContactDetails = $this->organisationRepository->getCurrentContactDetails($id);
        $contactDetailsByCountry = $this->groupContactDetailsByCountry($currentContactDetails);
        $people = $this->organisationRepository->getCurrentPeople($id);
        $peopleWithRoles = $this->hydratePeopleWithRoles($people, $id);
        return [
            'id' => $basicInfo['id'],
            'name' => $basicInfo['name'],
            'organisation_contacts' => $contactDetailsByCountry,
            'people' => $peopleWithRoles
        ];
    }

    /**
     * @param array $people
     * @param int $organisationId
     */
    private function hydratePeopleWithRoles(array $people, int $organisationId): array
    {
        $hydratedArray = [];
        foreach ($people as $person) {
            $role = $this->personRepository->getOrganisationRoleForPerson($person['id'], $organisationId);
            $person['job_title'] = $role['job_title'];
            $person['work_phone'] = $role['phone'];
            $hydratedArray[] = $person;
        }
        return $hydratedArray;
    }

    /**
     * Accept a single dimension list of contact details and return a map of country => list of details for that country
     * @param array $contactDetails
     * @return array
     */
    protected function groupContactDetailsByCountry(array $contactDetails): array
    {
        $returnArray = [];

        foreach ($contactDetails as $contactDetail) {
            $countryId = $contactDetail['country_id'];
            $countryName = $this->countryRepository->getCountryNameFromId($countryId);
            // initialise the map key if need be
            if(!isset($returnArray[$countryName])) {
                $returnArray[$countryName] = [];
            }
            // add detail to list under key, leaving country_id in place for consistency
            $returnArray[$countryName][] = $contactDetail;
        }
        return $returnArray;
    }

    /**
     * @inheritDoc
     */
    public function createWithAttributes(array $attributes): Organisation
    {
        event(new OrganisationCreated($attributes));
    }

    /**
     * @inheritDoc
     */
    public function linkOrganisationToCountry(int $organisationId, int $countryId): void
    {
        event(new LinkedOrganisationToCountry($organisationId, $countryId));
    }

    /**
     * @inheritDoc
     */
    public function unlinkOrganisationFromCountry(int $organisationId, int $countryId): void
    {
        event(new RemoveOrganisationFromCountry($organisationId, $countryId));
    }

    /**
     * @inheritDoc
     */
    public function addContactDetailsForOrganisation(int $organisationId, array $details, int $countryId): void
    {
        event(new OrganisationDetailedAdded($organisationId, $details, $countryId));
    }

    /**
     * @inheritDoc
     */
    public function changeContactDetailsForOrganisation(int $organisationId, array $details): void
    {
        event(new OrganisationDetailsAdded($organisationId, $details));
    }

    /**
     * @inheritDoc
     */
    public function removeContactDetailsForOrganisation(int $organisationId): void
    {
        event(new OrganisationDetailedChanged($organisationId, $details, $countryId));
    }
}