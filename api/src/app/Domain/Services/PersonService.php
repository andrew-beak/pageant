<?php

namespace App\Domain\Services;

use App\Domain\Interfaces\PersonServiceInterface;

class PersonService implements PersonServiceInterface
{
    /**
     * @inheritDoc
     */
    public static function changeContactDetails(int $id, array $newAttributes): void
    {
        event(new PersonDetailsChanged($id, $newAttributes));
    }

    public function remove(): void
    {
        event(new PersonDeleted($this->id));
    }

}