<?php

namespace App\Domain\Repositories;

use App\Domain\Interfaces\CountryRepositoryInterface;
use App\Domain\Persistence\Country;

class CountryRepository implements CountryRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function getCountryNameFromId(int $id): string
    {
        return Country::findOrFail($id)->name;
    }
}