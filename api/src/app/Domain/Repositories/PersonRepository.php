<?php

namespace App\Domain\Repositories;

use App\Domain\Interfaces\PersonRepositoryInterface;
use App\Domain\Persistence\Role;
use App\Domain\Persistence\Person;
use Illuminate\Database\Eloquent\Collection;

class PersonRepository implements PersonRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public static function changeContactDetails(int $id, array $newAttributes): void
    {
        event(new PersonDetailsChanged($id, $newAttributes));
    }

    public function remove(): void
    {
        event(new PersonDeleted($this->id));
    }

    /**
     * @inheritDoc
     */
    public function getOrganisationRoleForPerson(int $personId, int $organisationId): array
    {
        return Role::where('people_id', $personId)
            ->where('organisation_id', $organisationId)
            ->first()
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getPeople(): array
    {
        return Person::all()->toArray();
    }
}