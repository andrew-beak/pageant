<?php

namespace App\Domain\Repositories;

use App\Domain\Interfaces\OrganisationRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use App\Domain\Persistence\Organisation;
use App\Domain\Persistence\OrganisationContactDetail;

class OrganisationRepository implements OrganisationRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getOrganisations(): array
    {
        return Organisation::get()->toArray();
    }

    /**
     * @inheritdoc
     */
    public function getOrganisationById(int $id): array
    {
        return Organisation::findOrFail($id)->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getCurrentContactDetails(int $id): array
    {
        return Organisation::findOrFail($id)
            ->contactDetails()
            ->get()
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function getCurrentPeople(int $id): array
    {
        return Organisation::findOrFail($id)
        ->people()
        ->get()
        ->toArray();
    }
}