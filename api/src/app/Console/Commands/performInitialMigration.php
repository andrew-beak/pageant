<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class performInitialMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pageant:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate from the original, supplied schema to the new schema';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $numPeople = $this->createPeopleEvents();
        $this->info("I have added [$numPeople] events to create all the people");
        $numOrganisations = $this->createOrganisationEvents();
        $this->info("I have added [$numOrganisations] events to create all the organisations");
        $numPeople = $this->linkPeopleToOrganisations();
        $this->info("I have linked [$numPeople] to organisations");
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param array $attributes
     * @param string $eventTime
     * @param string $userName
     */
    protected function addEventForCreatingOrganisation(
        array $attributes,
        string $eventTime,
        string $userName
    ): void {
        DB::table('stored_events')->insert(
            [
                'aggregate_uuid' => null,
                'aggregate_version' => null,
                'event_class' => 'App\Events\OrganisationCreated',
                'event_properties' => json_encode(['attributes' => $attributes]),
                'meta_data' => json_encode(['user_name' => $userName]),
                'created_at' => $eventTime
            ]
        );
    }

    /**
     * @param int $organisationId
     * @param int $countryId
     * @param string $userName
     * @param string $eventTime
     */
    protected function addEventToLinkOrganisationToCountry(
        int $organisationId,
        int $countryId,
        string $userName,
        string $eventTime
    ): void {
        DB::table('stored_events')->insert(
            [
                'aggregate_uuid' => null,
                'aggregate_version' => null,
                'event_class' => 'App\Events\LinkedOrganisationToCountry',
                'event_properties' => json_encode(['organisationId' => $organisationId, 'countryId' => $countryId]),
                'meta_data' => json_encode(['user_name' => $userName]),
                'created_at' => $eventTime
            ]
        );
    }

    /**
     * @param array $contactDetails
     * @param int $organisationId
     * @param int $countryId
     * @param string $userName
     * @param string $eventTime
     */
    protected function addEventToAddOrganisationContact(
        array $contactDetails,
        int $organisationId,
        int $countryId,
        string $userName,
        string $eventTime
    ): void {
        DB::table('stored_events')->insert(
            [
                'aggregate_uuid' => null,
                'aggregate_version' => null,
                'event_class' => 'App\Events\OrganisationDetailsAdded',
                'event_properties' => json_encode(
                    [
                        'newDetails' => $contactDetails,
                        'organisationId' => $organisationId,
                        'countryId' => $countryId
                    ]
                ),
                'meta_data' => json_encode(['user_name' => $userName]),
                'created_at' => $eventTime
            ]
        );
    }

    /**
     * Create the events that will migrate the organisations
     */
    protected function createOrganisationEvents(): int
    {

        $organisations = DB::connection('legacy')->table('organisation')->get();

        $createdOrganisations = 0;
        foreach ($organisations as $organisation) {
            // create the organisation
            $attributes = [
                'id' => $organisation->id,
                'name' => $organisation->name
            ];

            $eventCreatedTime = date('Y-m-d H:i:s', strtotime($organisation->created_on));

            $eventModifiedTime = date('Y-m-d H:i:s', strtotime($organisation->updated_on));

            $creatingUser = $organisation->created_by;

            $this->addEventForCreatingOrganisation($attributes, $eventCreatedTime, $creatingUser, $eventModifiedTime);

            // link to country (we seed the table with the same id from the supplied data)
            $this->addEventToLinkOrganisationToCountry(
                $organisation->id,
                $organisation->country_id,
                $creatingUser,
                $eventModifiedTime
            );

            // add contact details
            $contactDetails = [
                'address1' => $organisation->address1,
                'address2' => $organisation->address2,
                'address3' => $organisation->address3,
                'city' => $organisation->city,
                'postal_code' => $organisation->postal_code,
                'weburl' => $organisation->web,
                'email' => $organisation->email,
                'phone' => $organisation->phone
            ];
            $this->addEventToAddOrganisationContact(
                $contactDetails,
                $organisation->id,
                $organisation->country_id,
                $creatingUser,
                $eventModifiedTime
            );

            $createdOrganisations++;
        }

        return $createdOrganisations;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * @param array $attributes
     * @param string $eventTime
     * @param string $userName
     * @return string
     */
    protected function addEventForCreatingPerson(array $attributes, string $eventTime, string $userName)
    {
        $attributes['uuid'] = (string)Uuid::uuid4();

        DB::table('stored_events')->insert(
            [
                'aggregate_uuid' => null,
                'aggregate_version' => null,
                'event_class' => 'App\Events\PersonCreated',
                'event_properties' => json_encode(['attributes' => $attributes]),
                'meta_data' => json_encode(['user_name' => $userName]),
                'created_at' => $eventTime
            ]
        );

        return $attributes['uuid'];
    }

    /**
     * @param string $eventTime
     * @param int $personId
     * @param string $userName
     */
    protected function addEventForUpdatingPerson(string $eventTime, int $personId, string $userName)
    {
        DB::table('stored_events')->insert(
            [
                'aggregate_uuid' => null,
                'aggregate_version' => null,
                'event_class' => 'App\Events\PersonDetailsChanged',
                'event_properties' => json_encode(['id' => $personId, 'newDetails' => []]),
                'meta_data' => json_encode(['user_name' => $userName]),
                'created_at' => $eventTime
            ]
        );
    }

    /**
     * Create the events that will migrate the people
     */
    protected function createPeopleEvents(): int
    {
        $contacts = DB::connection('legacy')->table('organisation_contact')->get();

        $numCreated = 0;

        foreach ($contacts as $contact) {
            $attributes = [
                'id' => $contact->id,
                'name' => $contact->name,
                'phone' => $contact->phone,
                'email' => $contact->email,
                'active' => (bool)$contact->active
            ];

            $eventCreatedTime = date('Y-m-d H:i:s', strtotime($contact->created_on));

            $creatingUser = $contact->created_by;

            $uuid = $this->addEventForCreatingPerson($attributes, $eventCreatedTime, $creatingUser);

            $eventUpdatedTime = date('Y-m-d H:i:s', strtotime($contact->updated_on));

            $updatingUser = $contact->updated_by;

            $this->addEventForUpdatingPerson($eventUpdatedTime, $contact->id, $updatingUser);

            $numCreated++;
        }
        return $numCreated;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------

    protected function addEventForAddingRole(
        string $userName,
        string $eventTime,
        int $personId,
        int $organisationId,
        ?string $jobTitle,
        ?string $phone
    ): void {

        DB::table('stored_events')->insert(
            [
                'aggregate_uuid' => null,
                'aggregate_version' => null,
                'event_class' => 'App\Events\LinkedPersonToOrganisation',
                'event_properties' => json_encode(
                    [
                        'personId' => $personId,
                        'organisationId' => $organisationId,
                        'phone' => $phone,
                        'jobTitle' => $jobTitle
                    ]
                ),
                'meta_data' => json_encode(['user_name' => $userName]),
                'created_at' => $eventTime
            ]
        );
    }

    /**
     * These events must be created only when the organisation and the person entities are already created
     */
    protected function linkPeopleToOrganisations()
    {
        $contacts = DB::connection('legacy')->table('organisation_contact')->get();

        $numCreated = 0;

        foreach ($contacts as $contact) {

            $eventUpdatedTime = date('Y-m-d H:i:s', strtotime($contact->updated_on));

            $updatingUser = $contact->updated_by;

            $this->addEventForAddingRole(
                $updatingUser,
                $eventUpdatedTime,
                $contact->id,
                $contact->organisation_id,
                $contact->title,
                $contact->phone,
            );

            $numCreated++;
        }

        return $numCreated;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Utility function for debugging
     * TODO remove this
     * @param string $tableName
     */
    private function listTable(string $tableName)
    {
        $rows = DB::table($tableName)->get();
        foreach ($rows as $row) {
            print_r($row);
        }
        die();
    }
}
