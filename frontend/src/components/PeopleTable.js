import React from 'react';
import {  withStyles } from '@material-ui/core';
import MaterialTable from 'material-table';
import { withRouter } from "react-router";
import { Check, Close } from '@material-ui/icons';

const styles = {
    table: {
        minWidth: 400,
      },
}

class PeopleTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            columns: [
                { title: "ID", field: "id" },
                { title: "Name", field: "name" },
                { title: "Phone (personal)", field: "phone"},
                { title: "Email", field: "email" },
                {
                    title: "Active",
                    field: "active",
                    render: rowData => rowData.active ? <Check /> : <Close />
                },
            ],
            items: []
        };
    }

    componentDidMount() {
        fetch("http://35.178.71.73/api/v1/people")
            .then(res => res.json())
            .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    data: result.data
                });
            },

            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render() {
        const { error, isLoaded } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <MaterialTable
                    columns={this.state.columns}
                    data={this.state.data}
                    title=""
                />
            );
        }
    }
}

export default withRouter(withStyles(styles)(PeopleTable));