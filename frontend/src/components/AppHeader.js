import React from 'react';
import { Link } from 'react-router-dom';
import {
  AppBar,
  Toolbar,
  Button,
  Typography,
  withStyles,
} from '@material-ui/core';

const styles = {
    flex: {
        flex: 1,
    },
};

const AppHeader = ({ classes }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="h6" color="inherit">
        Pageant code test
      </Typography>
      <div className={classes.flex} />
      <Button color="inherit" component={Link} to="/">Organisations</Button>
      <Button color="inherit" component={Link} to="/people">People</Button>
    </Toolbar>
  </AppBar>
);

export default withStyles(styles)(AppHeader);