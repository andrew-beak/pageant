import React, { Component } from 'react';
import {
    withStyles,
    Typography,
    Box,
    Link
} from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

const styles = {
    flex: {
        flex: 1,
    },
};

class OrganisationContactByCountry extends Component {

    mapObject(object, callback) {
        return Object.keys(object).map(function (key) {
            return callback(key, object[key]);
        });
    }

    render() {
        const classes = makeStyles(styles);
        console.log(this.props.data.organisation_contacts);
        return (
            <React.Fragment>
                <Typography variant="h5">Organisation contact by country</Typography>
                {Object.keys(this.props.data.organisation_contacts).map(key => (
                    <React.Fragment>
                        <Box component="span" m={8}>
                        <Typography variant="h6">{key}</Typography>
                            <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>ID</TableCell>
                                            <TableCell>Address1</TableCell>
                                            <TableCell>Address2</TableCell>
                                            <TableCell>Address3</TableCell>
                                            <TableCell>City</TableCell>
                                            <TableCell>Postal Code</TableCell>
                                            <TableCell>Email</TableCell>
                                            <TableCell>Phone</TableCell>
                                            <TableCell>WebURL</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                    {this.props.data.organisation_contacts[key].map((row) => (
                                        <TableRow>
                                            <TableCell>{row.id}</TableCell>
                                            <TableCell>{row.address1}</TableCell>
                                            <TableCell>{row.address2}</TableCell>
                                            <TableCell>{row.address3}</TableCell>
                                            <TableCell>{row.city}</TableCell>
                                            <TableCell>{row.postal_code}</TableCell>
                                            <TableCell>{row.email}</TableCell>
                                            <TableCell>{row.phone}</TableCell>
                                            <TableCell>
                                                <Link href={row.weburl} target="_blank" rel="noopener">
                                                    {row.weburl}
                                                </Link>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Box>
                    </React.Fragment>
                ))}
            </React.Fragment>
        );
    }
};

export default withStyles(styles)(OrganisationContactByCountry);