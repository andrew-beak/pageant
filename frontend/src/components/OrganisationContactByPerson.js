import React, { Component } from 'react';
import { withStyles, Typography } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Check, Close } from '@material-ui/icons';


const styles = {
    flex: {
        flex: 1,
    },
};

class OrganisationContactByPerson extends Component {

    mapObject(object, callback) {
        return Object.keys(object).map(function (key) {
            return callback(key, object[key]);
        });
    }

    render() {
        const classes = makeStyles(styles);
        console.log(this.props.data.organisation_contacts);
        return (
            <React.Fragment>
                <Typography variant="h5">Organisation contacts</Typography>
                    <React.Fragment>
                        <TableContainer component={Paper}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>ID</TableCell>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Phone (personal)</TableCell>
                                        <TableCell>Phone (work)</TableCell>
                                        <TableCell>Email</TableCell>
                                        <TableCell>Active</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                {this.props.data.people.map((row) => (
                                <TableRow>
                                    <TableCell>{row.id}</TableCell>
                                    <TableCell>{row.name}</TableCell>
                                    <TableCell>{row.phone}</TableCell>
                                    <TableCell>{row.work_phone}</TableCell>
                                    <TableCell>{row.email}</TableCell>
                                    <TableCell>
                                        {row.active
                                        ? <Check />
                                        : <Close />}
                                    </TableCell>
                                </TableRow>
                                ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </React.Fragment>
            </React.Fragment>
        );
    }
};

export default withStyles(styles)(OrganisationContactByPerson);