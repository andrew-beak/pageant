import React from 'react';
import {  withStyles } from '@material-ui/core';
import MaterialTable from 'material-table';
import { withRouter } from "react-router";

const styles = {
    table: {
        minWidth: 400,
      },
}

class OrganisationTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            columns: [
                { title: "ID", field: "id" },
                { title: "Name", field: "name" },
            ],
            items: []
        };
    }

    componentDidMount() {
        fetch("http://35.178.71.73/api/v1/organisations")
            .then(res => res.json())
            .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    data: result.data
                });
            },

            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render() {
        const { error, isLoaded } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <MaterialTable
                    columns={this.state.columns}
                    data={this.state.data}
                    title=""
                    actions={[
                        {
                          icon: 'details',
                          tooltip: 'View details',
                          onClick: (event, rowData) => {
                              this.props.history.push("/organisation/" + rowData.id);
                          }
                        }
                    ]}
                />
            );
        }
    }
}

export default withRouter(withStyles(styles)(OrganisationTable));