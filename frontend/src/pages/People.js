import React from 'react';
import {
  Typography,
  Box,
  Container
} from '@material-ui/core';

import PeopleTable from '../components/PeopleTable';

const People = ({ classes }) => (
  <React.Fragment>
    <Container maxWidth="lg">
      <Box component="span" m={8}>
        <Typography variant="h4">People</Typography>
        <PeopleTable />
      </Box>
    </Container>

  </React.Fragment>
);

export default People;