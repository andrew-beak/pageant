import React from 'react';
import {
  Typography,
  Box,
  Container
} from '@material-ui/core';

import OrganisationTable from '../components/OrganisationTable';

const Organisations = ({ classes }) => (
  <React.Fragment>
    <Container maxWidth="lg">
      <Box component="span" m={8}>
        <Typography variant="h4">Organisations</Typography>
        <OrganisationTable />
      </Box>
    </Container>
  </React.Fragment>
);

export default Organisations;