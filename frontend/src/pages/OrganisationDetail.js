import React, { Component } from 'react';
import {
  Typography,
  Divider,
  Box
} from '@material-ui/core';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';


import OrganisationContactByCountry from '../components/OrganisationContactByCountry';
import OrganisationContactByPerson from '../components/OrganisationContactByPerson';

class OrganisationDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: {}
        };
    }

    componentDidMount() {
        fetch("http://35.178.71.73/api/v1/organisations/" + this.props.match.params.id)
            .then(res => res.json())
            .then(
            (result) => {
                this.setState({
                    isLoaded: true,
                    data: result.data
                });
            },

            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        )
    }

    render() {
        const { error, isLoaded } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return (
                <CircularProgress />
            );
        } else {
            return (
                <React.Fragment>
                    <Container maxWidth="lg">
                        <Typography variant="h4">{this.state.data.name}</Typography>
                        <Divider />
                        <Box component="span" m={8}>
                            <OrganisationContactByCountry data={this.state.data} />
                        </Box>
                        <Box component="span" m={8}>
                            <OrganisationContactByPerson data={this.state.data} />
                        </Box>
                    </Container>
                </React.Fragment>
            );
        }
    }
}

export default OrganisationDetail;