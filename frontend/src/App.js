import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import {
  CssBaseline,
  withStyles,
} from '@material-ui/core';

import AppHeader from './components/AppHeader';
import Organisations from './pages/Organisations';
import OrganisationDetail from './pages/OrganisationDetail';
import People from './pages/People';

const styles = theme => ({
  main: {
    padding: theme.spacing(3),
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing(2),
    },
  },
});

const App = ({ classes }) => (
  <Fragment>
    <CssBaseline />
    <AppHeader />
    <main className={classes.main}>
       <Route exact path="/" component={Organisations} />
       <Route exact path="/people" component={People} />
       <Route path="/organisation/:id" render={(props) => <OrganisationDetail {...props} /> } />

     </main>
  </Fragment>
);

export default withStyles(styles)(App);